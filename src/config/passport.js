const passport = require('passport');
const LocalStrategy = require('passport-local');

const User = require ('../models/User');
const Audit = require('../models/Audit');

passport.use(
	new LocalStrategy(
	{
		usernameField: 'email',
		passwordField: 'password'
	}, 
	async (email, password, done) => {

    const user =  await User.findOne({email})
    
	if (!user)
    {
        return done(null, false, {message : 'Not User Found'});
    }
    else
    {
        const macht = await user.matchPassword(password)
        
        if (macht)
            {      
                //register Audit event session lognin
                //console.log('Login Message ');
                const msglog = 'Login Message '+user.email;
                const userid = user.id;
                const newAudit = new Audit ({userid, msglog});
                await newAudit.save();

                return done(null, user)
            }
        else 
            {
                return done(null, false, {message: 'Incorect Password'})
            }
    }
    
 
}));

passport.serializeUser ((user, done) => {
    done(null, user.id);
});

passport.deserializeUser ((id, done) => {
    User.findById(id, (err, user) => {
        done(err, user);
    });
});