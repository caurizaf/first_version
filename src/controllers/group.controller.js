const groupCtrl = {};


const Group = require('../models/Group');

const groups = require('../json/group.json');


groupCtrl.renderGroupForm = (req,res) => {
    //res.send('notes add');
    res.render('group/new-group');
};

groupCtrl.createNewGroup = (req,res) => {

    const {title, description} = req.body;
  
    if (title && description )
      {  
          const id = groups.length + 1 ;
          const new_Group = {id, ...req.body};
          groups.push(new_Group);
          //console.log(groups);
  
          res.render('groups/all-group',{groups});
      }
      else
      {
          res.status(500).json({error: 'There was an error.', wrong_data: req.body });
          res.render('groups/all-group',{groups});
      }
};

groupCtrl.renderGroup = (req,res) => {
    //res.send('Render group');
    res.render('group/all-group',{groups});
};

groupCtrl.renderGroupEditForm = (req,res) => {
    res.send('Render Form edit group');
    //res.render('index')
};

groupCtrl.updateGroup = (req,res) => {
    res.send('Update Form edit group');
    //res.render('index')
};

groupCtrl.deleteGroup = (req,res) => {
    res.send('delete group');
    //res.render('index')
};
module.exports = groupCtrl;