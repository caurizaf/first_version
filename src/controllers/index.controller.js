const indesxCtrl = {};

indesxCtrl.renderIndex = (req,res) => {
    res.render('index')
};

indesxCtrl.renderAbout = (req,res) => {
    res.render('partials/about')
};

module.exports = indesxCtrl;