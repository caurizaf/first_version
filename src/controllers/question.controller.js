
const questionCtrl = {};

const Question = require('../models/Question');




  questionCtrl.renderQuestion = async (req,res) => {

      const question = await Question.find().sort({createdAt: 'desc'});
      const countDocumentQuery = await Question.find().countDocuments().exec();
      res.render('questions/all-question',{question,countDocumentQuery});
     };
  
   
questionCtrl.renderQuestionForm = async (req,res) => {
      //const notes = await Note.find({user: req.user.id}).sort({createdAt: 'desc'});
      console.log('question');
      res.render('questions/all-question');
   
   };
  
questionCtrl.createNewQuestion = async (req,res) => {
      console.log('');
   };
  
  
questionCtrl.renderQuestionEditForm = async (req,res) => {
     console.log('');
   };

questionCtrl.updateQuestion = async (req,res) => {
     console.log('');
   };
  
questionCtrl.deleteAnswer = async (req,res) => {
     console.log('');
   };
  
   module.exports = questionCtrl;

   /*
   
   questionCtrl.loadAnswerdb  =  async (req, res) => {  
  
    const newQuestion1 = new Question ({ question : 1, text :'Esto es la pregunta 1', type_q : 1, imageExplain:'C:', explain:'Es la respues porque es la correcta' , answers:0 });
    const newQuestion2 = new Question ({ question : 2, text :'Esto es la pregunta 1', type_q : 1, imageExplain:'C:', explain:'Es la respues porque es la correcta' , answers:0 });
    const newQuestion3 = new Question ({ question : 3, text :'Esto es la pregunta 1', type_q : 1, imageExplain:'C:', explain:'Es la respues porque es la correcta' , answers:0 });
  
    console.log(newQuestion1);
  
  
    const newAnswer1 = new Answer ({ question: 1, text :'esta es la respuesta correcta', correctOption :true });
  
    const newAnswer2 = new Answer ({ question: 1, text :'esta no es la respuesta correcta', correctOption :false });
  
    const newAnswer3 = new Answer ({ question: 2, text :'esta es la respuesta correcta', correctOption :true });
  
    const newAnswer4 = new Answer ({ question: 2, text :'esta no es la respuesta correcta', correctOption :false });
    
    const newAnswer5 = new Answer ({ question: 3, text :'esta es la respuesta correcta', correctOption :true });
  
    const newAnswer6 = new Answer ({ question: 3, text :'esta no es la respuesta correcta', correctOption :false });
    
    console.log(newAnswer1);
    console.log(newAnswer2);
  
    await newQuestion1.save();
    await newAnswer1.save();
    await newAnswer2.save();
    await newQuestion2.save();
    await newAnswer3.save();
    await newAnswer4.save();
    await newQuestion3.save();
    await newAnswer5.save();
    await newAnswer6.save();
    //req.flash('succes_msg','Note Added Succesfully'); 
    //res.redirect('/notes');
  
  }
   */