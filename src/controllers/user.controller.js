const usersCtrl = {};

const passport = require('passport');

const User = require('../models/User');
const Audit = require('../models/Audit');

usersCtrl.renderSignUpForm = (req,res) => {
    //res.send('notes add');
    res.render('users/signup');
};

usersCtrl.signUp= async (req, res) => 
{   
   const errors = []; 
   const {name, surname, phone, email, password, confirm_password} = req.body;

   console.log(name, surname, phone, email, password );

   if (password != confirm_password)
    {   
        errors.push({text: 'Password do not match'})
        
    }
    if (password.length < 8) 
    {
        errors.push({text: 'Password must be at least 8 character'})
    }
    if (errors.length > 0 ){

        res.render('users/signup', 
        {
        errors,
        name,
        surname,
        phone,
        email,
        password,   
        confirm_password
        });
    }
    else {

        const emailUser = await User.findOne({email: email});
        if (emailUser)
        {
            req.flash('error_msg','the email is already in use.')
            res.redirect('/users/signup');
        }
        else 
        {
            const newUser = new User ({name, surname, phone, email, password });
            newUser.password = await newUser.encrypPassword(password);
            newUser.save();
            req.flash('succes_msg','User Added Succesfully'); 
            res.redirect('/users/signin');
        }
   

        //res.send('signup succesfully')
    }
   
   // res.send('signup');
};

usersCtrl.renderSignInForm= (req, res) =>
{
    res.render('users/signin');
};

usersCtrl.signIn = passport.authenticate('local', 
{
    failureRedirect: '/users/signin',
    successRedirect: '/notes',
    failureFlash: true

});

usersCtrl.logOut = async (req, res) =>
{   
    //register Audit event session logout
    const {_id, _name, surname, phone, email, password, createAt, updateAt} = req.user;
    console.log('Logout Message ' + _id);
    console.log('Logout Message ' + email);
    const msglog = 'Logout Message '+ email;
    const userid = _id;
    const newAudit = new Audit ({userid, msglog});
    await newAudit.save();
    //end register Audit event session logout  
    req.logOut();
    
    res.redirect('/users/signin');
    req.flash('success_msg','You are logged out now.');
};

module.exports = usersCtrl;


