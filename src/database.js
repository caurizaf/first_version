const mongoose = require('mongoose');

const { NOTES_APP_MONGODB_HOST, NOTES_APP_MONGODB_DATABASE, NOTES_APP_MONGODB_PORT } = process.env;


//const NOTES_APP_MONGODB_HOST = process.env.NOTES_APP_MONGODB_HOST; //'mongodb://localhost:27017/notes-app'; 
//const NOTES_APP_MONGODB_PORT = process.env.NOTES_APP_MONGODB_PORT;
//const NOTES_APP_MONGODB_DATABASE = process.env.NOTES_APP_MONGODB_DATABASE;

const MONGODB_URI = 'mongodb://localhost:27017/notes-app'; // mongodb://' + NOTES_APP_MONGODB_HOST + '/' + NOTES_APP_MONGODB_DATABASE ;

//console.log(MONGODB_URI);
//console.log(NOTES_APP_MONGODB_HOST);
//console.log(NOTES_APP_MONGODB_DATABASE);

mongoose.connect('mongodb://0.0.0.0:27017/notes-app',{
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true
}) 
    .then(db => console.log('Database is Connect'))
    .catch(err => console.log(err));

