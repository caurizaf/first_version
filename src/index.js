require('dotenv').config;

const app = require ('./server');
require('./database');

//initialization
const { NOTES_APP_MONGODB_HOST, NOTES_APP_MONGODB_DATABASE } = process.env;

//console.log(process.env.TESTING);
//console.log('mongodb://${NOTES_APP_MONGODB_HOST}/${NOTES_APP_MONGODB_DATABASE}');
//console.log(NOTES_APP_MONGODB_DATABASE, NOTES_APP_MONGODB_HOST);
//console.log(process.env.NOTES_APP_MONGODB_DATABASE, process.env.NOTES_APP_MONGODB_HOST);

app.listen(app.get('port'), () => {
    //console.log('Server on port ', app.get('port'));
});

//Settings

//Middlewares

//Global Variables

//Routes

//static files

// server listener
