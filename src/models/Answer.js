const {Schema, model} = require('mongoose');

const AnswerSchema = new Schema(
    {
    question: {
        type: Number,
        required: true
    },
    text: {
        type: String,
        required: true
    },
    correctOption: {
        type: Boolean,
        required: true
    }
        
    },
    {    
        timestamps:true
    });

module.exports = model('Answer', AnswerSchema)