const {Schema, model} = require('mongoose');

const AuditSchema = new Schema(
    {
    userid:{
        type: String,
        required: true},

    msglog:{
        type: String,
        required: true}
    },{
    timestamps:true
    })

module.exports = model('Audit', AuditSchema)
     