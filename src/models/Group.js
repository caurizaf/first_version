const {Schema, model} = require('mongoose');

const GroupSchema = new Schema(
    {
    title:{
        type: String,
        required: true},

    description:{
        type: String,
        required: true}
    },{
    timestamps:true
})

model.export = model('Group', GroupSchema)
