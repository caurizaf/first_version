const {Schema, model} = require('mongoose');

const QuestionSchema = new Schema(
    {
    question: {
        type: Number,
        required: true
    },
    text:{
        type: String,
        required: true,
    },
    type_q: {
        type: Number,
        required: true
    },
    imageExplain:{
        type: String,
        required: true,
    },
    explain:{
        type: String,
        required:false
    },
    answers:{
        type: Number,
        required:true
    }
    
    },
    {    
        timestamps:true
    });

module.exports = model('Question', QuestionSchema)
