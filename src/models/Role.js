const {Schema, model} = require('mongoose');

const RoleSchema = new Schema({
    title:{
        type: String,
        required: true},

    description:{
        type: String,
        required: true}
},{
    timestamps:true
})


model.export = model('group', RoleSchema)
