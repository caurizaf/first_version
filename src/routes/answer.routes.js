const {Router} = require('express');
const router = Router();

const Answer = require('../models/Answer')

const {
    renderAnswer,
    renderAnswerForm,
    renderAnswerEditForm
} = require('../controllers/answer.controller');

const {isAuthenticated}  = require('../helpers/auth');

router.get('/answer',isAuthenticated, renderAnswer);
router.get('/answer/new-Answer',isAuthenticated, renderAnswerForm);

module.exports = router;