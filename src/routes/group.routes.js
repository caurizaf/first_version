const {Router} = require('express');
const router = Router();


const { 
    renderGroupForm ,
    createNewGroup, 
    renderGroup, 
    renderGroupEditForm, 
    updateGroup,
    deleteGroup } = require('../controllers/group.controller');
    
//New Group
router.get ('/groups/add', renderGroupForm);
router.post ('/groups/new-group', createNewGroup);

//Get all Groups
router.get ('/groups', renderGroup);

//edit Groups
router.get('/groups/edit/:id', renderGroupEditForm);
router.put('/groups/edit/:id', updateGroup);

//delete Group
router.put('/groups/delete/:id', deleteGroup);

module.exports = router;