const {Router} = require('express');
const router = Router();

const { renderAbout,renderIndex } = require('../controllers/index.controller')

router.get('/', renderIndex);
router.get('/about',renderAbout);

/*
router.get('/',(req,res) => {
    res.render('index');
    // res.send('Hello Word');
});*/


/*
router.get('/about',(req,res) => {
    res.render('partials/about.hbs');
    
});
*/
module.exports = router;
