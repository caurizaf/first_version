const {Router} = require('express');
const router = Router();
const Note = require('../models/Note');

const { 
    renderNoteForm ,
    createNewNote, 
    renderNotes, 
    renderNoteEditForm, 
    updateNote,
    deleteNote
    
     } = require('../controllers/note.controller');
 
const {isAuthenticated}  = require('../helpers/auth');

//New note
router.get ('/notes/add',isAuthenticated, renderNoteForm);

router.post ('/notes/new-note',isAuthenticated, createNewNote);
//get All Note
router.get ('/notes',isAuthenticated, renderNotes);
//edit notes
router.get('/notes/edit/:id',isAuthenticated, renderNoteEditForm);
router.put('/notes/edit/:id',isAuthenticated, updateNote);

//delete note
router.delete('/notes/delete/:id',isAuthenticated, deleteNote);

module.exports = router;