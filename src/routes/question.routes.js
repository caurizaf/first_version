const {Router} = require('express');
const router = Router();

const Answer = require('../models/Answer')

const {
    renderQuestion,
    renderQuestionForm,
    renderQuestionEditForm
} = require('../controllers/question.controller');

const {isAuthenticated}  = require('../helpers/auth');

router.get('/question',isAuthenticated, renderQuestion);
router.get('/question/new-question',isAuthenticated, renderQuestionForm);

module.exports = router;