const {Router} = require('express');
const router = Router();


const {renderSignUpForm, renderSignInForm, signUp, signIn, logOut} = require('../controllers/user.controller');

//router.get('/users', renderSignUpForm);
router.get('/users/signup', renderSignUpForm);
router.post('/users/signup', signUp);
router.get('/users/signin', renderSignInForm);
router.post('/users/signin', signIn);
router.get('/users/logout',logOut);


module.exports = router;


/*
const { 
    renderUserForm ,
    createNewUser, 
    renderUser, 
    renderUserEditForm, 
    updateUser,
    deleteUser
        } = require('../controllers/user.controller');
    
//New user
router.get ('/users/add', renderUserForm);
router.post ('/users/new-user', createNewUser);

router.get ('/users', renderUser);

//edit users
router.get('/users/edit/:id', renderUserEditForm);
router.put('/users/edit/:id', updateUser);

//delete user
router.put('/users/delete/:id', deleteUser);

*/