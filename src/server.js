//https://tsh.io/blog/how-to-write-video-chat-app-using-webrtc-and-nodejs/
const express =require('express');
const Handlebars = require('handlebars');
const exphbs = require('express-handlebars');

// Import function exported by newly installed node modules.
const { allowInsecurePrototypeAccess } = require('@handlebars/allow-prototype-access');

const path = require('path');
const morgan =  require('morgan');
const methodOverride = require('method-override');

const flash = require('connect-flash');
const session = require('express-session');
const passport = require ('passport');

//initialization
const app = express();
require('./config/passport')
const defviews = __dirname + '\\views'; //define defviews manual configuration for partialsDir, layoutsDir
const defpartialDir = path.join(defviews, "partials");
const deflayoutsDir = path.join(defviews, "layouts");
//console.log(defviews);
//console.log(defpartialDir);
//console.log(deflayoutsDir);

var hbs = exphbs.create({
    defaultLayout: "main",
    partialsDir: defpartialDir,    
    layoutsDir: deflayoutsDir,
    extname: ".hbs",
    handlebars: allowInsecurePrototypeAccess(Handlebars)
});

//Settings

app.set ('port',process.env.PORT || 4000 );
console.log(defviews);
app.set('views', path.join(__dirname ,'views'));
app.engine('.hbs',hbs.engine);


/*
app.engine( ".hbs", exphbs({
      defaultLayout: "main",
      layoutsDir: path.join(app.get("views"), "layouts"),
      partialsDir: path.join(app.get("views"), "partials"),
      extname: ".hbs"
    })
  );
*/
  //app.set("view engine", ".hbs");

app.set('view engine', '.hbs');

//Middlewares

app.use(morgan('dev'));
app.use(express.urlencoded({extended:false}));
app.use(methodOverride('_method'));
app.use(session({
  secret: 'secret',
  resave: false,
  saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

//Global Variables

app.use((req, res, next) =>{
  res.locals.success_msg = req.flash('succes_msg');
  res.locals.error_msg = req.flash('error_msg');
  res.locals.error = req.flash('error');
  res.locals.user = req.user || null;
  next();
});
//Routes
app.use(require('./routes/index.routes'));
app.use(require('./routes/note.routes'));
app.use(require('./routes/group.routes'));
app.use(require('./routes/user.routes'));
app.use(require('./routes/answer.routes'));
app.use(require('./routes/question.routes'));

app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(express.static('public'));
//static files

app.use(express.static(path.join(__dirname, 'public')));
app.use((req, res, next) => {
  return res.status(404).render("404");
});

module.exports = app;