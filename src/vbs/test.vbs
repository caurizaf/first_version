//-------------------------------------------
// RealConnect for Teams Preliminary Script
// 
// This preliminary script supports the RealConnect for Teams feature by allowing the user to dial the Teams conference
// with the following criteria:
//
//  1) Conference ID format (<conferenceID> or <conferenceID>[@<domain>] or full conference format (<tenantID>.<conferenceID>[@<domain>])
//  2) H.323 or SIP
//  3) Registered or unregistered endpoints (For unregistered endpoints, configure the Call Server Settings to allow calls 
//        from unregistered endpoints.)
// 
// To use this script, edit the variables that are commented with the word 'EDIT' to match your Teams site requirements. 
// After editing, copy and paste the script into the Preliminary script area of the 'Resolve to conference room with autodial' dial rule.
//-------------------------------------------

 

// <-- Copy the lines below here and paste into the Preliminary script area of the 'Resolve to conference room with autodial' dial rule -->

 

//-------------------------------------------
// User Configurable Values - Start
//-------------------------------------------
//
// These three variables are for Teams conference calls with only the
// conference ID in the dial string. Edit the value of these variables 
// to match your site requirements.
//

 

// EDIT: The length of the conference IDs assigned to your tenant ID
var conferenceIDLength = 10; 
// EDIT: The Teams tenant ID assigned to your subscription
var tenantID = '881318836';   
// EDIT: The domain assigned to your Teams tenant.   
var domainID = 't.plcm.vc';

 

// These three variables are for processing the full Teams conference 
// with autodial dial string "<tenant id>.<conference id>@<domain>"
// Customize them for your site or leave them as the defaults.  These 
// values are javascript regex strings.  The default values provided 
// will accept any value with the correct format.   

 

// EDIT: The Teams tenant ID assigned to your subscription or a regex that 
// encompasses the list of tenant IDs.
var tenantIdMatchStr = '[^:#*.]+'; 
// EDIT: A regex that encompasses the list of conference IDs possible for 
// your site.
var roomIdMatchStr = '[^:#*.]+';    
// EDIT: The domain for your subscription or a regex that encompasses the 
// list of domains.
var domainMatchStr = '.*vc';      

 

// Please use the "Debug this Script" tool to verify that the changes to 
// the values are working as expected.
//-------------------------------------------
// User Configurable Values - End
//-------------------------------------------

 

DIAL_STRING = processDialString(DIAL_STRING);

 

if (DIAL_STRING === 'REJECT')
{
       return NEXT_RULE;
}

 

//-------------------------------------------
// Main function
//-------------------------------------------

 

function processDialString(dialString)
{
    dialString = checkForConferenceOnly(dialString);

 

    if (dialString.indexOf('@') != -1 && !matchesDialPatterns(dialString)) 
    { 
        // For unregistered users dialing the full Teams string, the domain may not match the pattern.  Replace it 
        // with the specified Conference ID-only 'domainID' and try again to see if it's a match.
        dialString = transformUnregisteredDialString(dialString);
    } 
    
    if (!matchesDialPatterns(dialString)) 
    {
         return 'REJECT';
    } 

 

    return dialString;
}

 

//-------------------------------------------
// Supporting functions
//-------------------------------------------

 

function transformUnregisteredDialString(dialString)
{
    var domainPart='@' + domainID; 
    var matchPattern = '\s*(sip:|sips:|[hH]323:)?\s*(autodial-|partadial-)?' + tenantID + '\s*.*$';

 

    if (dialString.match(matchPattern)) 
    {
        if (dialString.indexOf('@') == -1) 
        {
            dialString = dialString + domainPart;
        }
        else 
        {
            dialString = dialString.replace(/^([^@]*)@(.*)/i, "$1" + domainPart);
        }
    }

 

    return dialString;
}

 

function matchesDialPatterns(dialString)
{
    var atPattern = '\\s*(sip:|sips:|[hH]323:)?\\s*(autodial-|partadial-)?' + tenantIdMatchStr + '[*.]' + roomIdMatchStr + '@' + domainMatchStr + '\\s*[:;?]?.*$'; 
    var specialDialPattern = '\\s*(sip:|sips:|[hH]323:)?\\s*(autodial-|partadial-).*'; 
    var poundPattern = '\\s*[#]*' + domainMatchStr + '[#]{2}' + tenantIdMatchStr + '#' + roomIdMatchStr + '[#]*\\s*$';

 

    if (!isMatch(atPattern, dialString) && !isMatch(specialDialPattern, dialString) && !isMatch(poundPattern, dialString))
    {
        return false;
    }

 

    return true;
}

 

function isMatch(pattern, stringToMatch)
{
     return (stringToMatch.match(pattern) !== null);
}

 

function checkForConferenceOnly(dialString)
{
    var indexOfDot = dialString.indexOf('.');
    var indexOfAt = dialString.indexOf('@');
    var matchConferenceIdOnlyPattern = '\\s*(sip:|sips:|[hH]323:)?\\s*\\d{' + conferenceIDLength + '}\\s*.*$';
    var atConferenceIdOnlyPattern = '\\s*(sip:|sips:|[hH]323:)?\\s*\\d{' + conferenceIDLength + '}@[^:#*]+\\s*[:;?]?.*$'; 

 

    if (indexOfDot == -1 && indexOfAt == -1 && dialString.match(matchConferenceIdOnlyPattern))
    {
        dialString = transformDialString(dialString);
    }
    else if (indexOfAt != -1) 
    {
        var beforeDomainPart = dialString.substring(0, indexOfAt);
        if (beforeDomainPart.indexOf('.') == -1 && dialString.match(atConferenceIdOnlyPattern)) 
        {
            dialString = transformDialString(dialString);
        }
    }

 

    return dialString;
}

 

function transformDialString(dialString)
{
    var dotPart = '.';
    var domainPart='@' + domainID; // The domain assigned to your Teams tenant 
    var matchPattern = '\\s*(sip:|sips:|[hH]323:)?\\s*\\d{' + conferenceIDLength + '}\\s*.*$';
    var atPattern = '\\s*(sip:|sips:|[hH]323:)?\\s*\\d{' + conferenceIDLength + '}@[^:#*.]+\\s*[:;?]?.*$'; 
    var prefixPart = '';
    var conferenceID = dialString;
    var indexOfColon = dialString.indexOf(':');
    var indexOfAt = dialString.indexOf('@');

 

    if (dialString.match(atPattern)) 
    {
       if (indexOfColon != -1) 
        {
            prefixPart = dialString.substring(0, indexOfColon + 1);
            conferenceID = dialString.substring(indexOfColon + 1, indexOfAt);
        }
        else 
        {
            conferenceID = dialString.substring(0, indexOfAt);
        }

 

        if (conferenceID.length == conferenceIDLength) 
        {
            dialString = prefixPart + tenantID + dotPart + conferenceID + domainPart;
        } 
    }
    else if (dialString.match(matchPattern))
    {
       if (indexOfColon != -1)
        {
            prefixPart = dialString.substr(0, indexOfColon + 1);
            conferenceID = dialString.substr(indexOfColon + 1);
        }

 

        if (conferenceID.length == conferenceIDLength)
        {
           dialString = prefixPart + tenantID + dotPart + conferenceID + domainPart;
        }
    }

 

    return dialString;
}